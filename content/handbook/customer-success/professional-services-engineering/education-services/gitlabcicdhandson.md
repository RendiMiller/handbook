---
title: "GitLab CI/CD - Hands-On Lab Overview"
description: "This Hands-On Guide walks you through the lab exercises in the GitLab CI/CD course."
---
# GitLab CI/CD Hands-On Guide

## Lab Guides

> **We are transitioning to the latest version of this course.** If your group URL starts with `https://spt.gitlabtraining.cloud`, please use the v15 instructions.


| Lab Guide | Version 15 | Version 16 |
|-----------|------------|------------|
| Lab 1: Review example GitLab CI/CD section | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab1.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab1) |
| Lab 2: Create a project containing a `.gitlab-ci.yml` file and register a GitLab Runner | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab2.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab2) |
| Lab 3: Create a basic CI configuration | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab3.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab3) |
| Lab 4: Display pipeline environment info | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab4.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab4) |
| Lab 5: Variable hierarchy | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab5.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab5) |
| Lab 6: Job policy pattern | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab6.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab6) |
| Lab 7: Using artifacts | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab7.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab7) |
| Lab 8: GitLab docker registry | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab8.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab8) |
| Lab 9: Security scanning | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab9.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab9) |
| Lab 9 (alternative): Code quality scanning | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab9alt.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitlabcicdhandsonlab9alt) |

## Quick links

Here are some quick links that may be useful when reviewing this Hands-On Guide.

* [GitLab CI/CD Course Description](https://about.gitlab.com/services/education/gitlab-ci/)
* [GitLab CI/CD Specialist Certification Details](https://about.gitlab.com/services/education/gitlab-cicd-associate/)

## Suggestions?

If you wish to make a change to the *Hands-On Guide for GitLab CI/CD*, please submit your changes via Merge Request!
